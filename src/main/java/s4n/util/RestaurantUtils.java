package s4n.util;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import s4n.corrientazo.builders.OrderBuilder;
import s4n.corrientazo.enums.ECustomMessageException;
import s4n.corrientazo.exception.CorrientazoException;
import s4n.corrientazo.model.Order;
import s4n.corrientazo.model.Schedule;

/**
 * Used as a util class that contains utilities methods
 * 
 * @author Carlos Torres
 *
 */
public class RestaurantUtils {

	private RestaurantUtils() {
	}

	/**
	 * Charges all the scheduled created for the restaurant
	 * 
	 * @param capacitySimultaneousSend used to charge one specific number of
	 *                                 schedules.
	 * @return list schedule configured for the restaurant
	 */
	public static List<Schedule> getScheduleList(int capacitySimultaneousSend,String pathReadSchedules) {
		List<Schedule> scheduleList = new ArrayList<>();
		for (int counter = 1; counter <= capacitySimultaneousSend; counter++) {
			Path pathSchedule = Paths.get(pathReadSchedules + "\\" + getInString(counter) + ".txt");
			if (Files.exists(pathSchedule, LinkOption.NOFOLLOW_LINKS)) {
				scheduleList.add(getSchedule(pathSchedule,counter));
			}
		}
		return scheduleList;
	}

	/**
	 * Write the report for a specific schedule
	 * 
	 * @param schedule used to write the report
	 * @param pathWriteReport used to indicate where the reports are going to be store
	 */
	public static void writeReportSchedule(Schedule schedule, String pathWriteReport) {

		String fileContent = schedule.getOrderList().stream().map(Order::getAddress)
				.collect(Collectors.joining(System.lineSeparator()));

		fileContent = "== Reporte de entregas ==" + System.lineSeparator() + fileContent;

		try (FileWriter fileWriter = new FileWriter(pathWriteReport+getOutString(schedule.getNumberSchedule()) + ".txt");) {
			fileWriter.write(fileContent);
			System.out.println("Se escribio reporte: " + fileContent + "\nEn " + pathWriteReport);
		} catch (IOException e) {
			throw new CorrientazoException(ECustomMessageException.ERROR_WRITE_FILE.getMessage());
		}
	}

	/**
	 * Gets the schedule with base in a specific pathFile
	 * 
	 * @param pathFile used to read all the indication lines
	 * @return schedule object
	 */
	private static Schedule getSchedule(Path pathFile,int counter) {
		List<Order> orderList = readLineByLine(pathFile).stream().map(orderIndication -> {
			return new OrderBuilder().setAddress("").setIndications(getCharacterList(orderIndication.toCharArray()))
					.setOrderPlaced(false).build();
		}).collect(Collectors.toList());

		return new Schedule(orderList,counter);
	}

	/**
	 * Used to read a specific file path line by line
	 * 
	 * @param filePath to be read
	 * @return list of lines in the file
	 */
	private static List<String> readLineByLine(Path filePath) {
		List<String> lines = new ArrayList<>();
		try (Stream<String> streamLines = Files.lines(filePath, StandardCharsets.UTF_8)) {
			streamLines.forEach(lines::add);
		} catch (IOException e) {
			throw new CorrientazoException(ECustomMessageException.ERROR_READ_FILE.getMessage());
		}
		return lines;
	}

	/**
	 * Gets the string value number that represents one schedule
	 * 
	 * @param numero to use to represent an index
	 * @return string value
	 */
	private static String getInString(int number) {
		return "in" + (number < 10 ? "0" : "") + number;
	}

	/**
	 * Gets the character list of the specific char array
	 * 
	 * @param charArray {@link char[]} to become to character list
	 * @return character list
	 */
	private static List<Character> getCharacterList(char[] charArray) {
		List<Character> characterList = new ArrayList<>();
		for (char value : charArray) {
			characterList.add(value);
		}
		return characterList;
	}
	
	/**
	 * Gets the string value number that represents one schedule
	 * 
	 * @param numero to use to represent an index
	 * @return string value
	 */
	private static String getOutString(int number){
		return "out"+(number<10?"0":"")+number;
	}

}
