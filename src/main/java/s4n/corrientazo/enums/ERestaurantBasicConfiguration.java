package s4n.corrientazo.enums;

/**
 * Define a basic restaurant configuration
 * @author Carlos Torres
 *
 */
public enum ERestaurantBasicConfiguration {

	
	CAPACITY_SIMULTANEOUS_SEND(20),
	MAXIMUM_REMOTE(10),
	MAXIMUM_ORDER_BY_DRONE(3);
	
	private int value;
	
	private ERestaurantBasicConfiguration(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
}
