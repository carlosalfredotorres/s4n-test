package s4n.corrientazo.enums;

/**
 * Define the custom message exception
 * @author Carlos Torres
 *
 */
public enum ECustomMessageException {

	ORDERS_EXCEEDED("Excede el numero de pedidos maximos que puede tener un dron, cantidad maxima: "),
	ERROR_READ_FILE("Error a la hora de procesar el archivo"),
	ERROR_WRITE_FILE("Error a la hora de escribir el archivo"),
	ERROR_LIMIT_EXCEEDED("El Dron no puede ir tan lejos"),
	ERROR_NO_SCHEDULE("No existen pedidos"),
	ERROR_NO_RESTAURANT_CONFIGURATION("No existen una configuración de envios para el restaurante"),
	ERROR_UKNOW("Error desconocido en el momento de mandar los drones");

	private String message;

	private ECustomMessageException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
