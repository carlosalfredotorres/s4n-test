package s4n.corrientazo.enums;

/**
 * Used to get the urls where it is going to be the input and output files
 * @author Carlos Torres
 *
 */
public enum EUrlInputOutput {

	URL_INPUT("src\\main\\resources\\in\\"),
	URL_OUTPUT("src\\main\\resources\\out\\");
	
	private String value;
	
	private EUrlInputOutput(String value) {
		this.value= value;
	}
	
	public String getValue() {
		return value;
	}
	
}
