package s4n.corrientazo.enums;

/**
 * Define the orientation for the drone
 * @author Carlos Torres
 *
 */
public enum EOrientation {

	NORTH{
		@Override
		public String toString() {
			return "dirección Norte"; 
		}

	},
	SOUTH{
		@Override
		public String toString() {
			return "dirección Sur"; 
		}
	},
	EAST{
		@Override
		public String toString() {
			return "dirección Oriente"; 
		}
	},
	WEST{
		@Override
		public String toString() {
			return "dirección Occidente"; 
		}
	};
	
	/**
	 * Return the result of made a turn left over the received orientation
	 * 
	 * @param receivedOrientation {@link EOrientation} used to turn left
	 * @return new orientation after turn left
	 */
	public static EOrientation turnLeft(EOrientation receivedOrientation){
		switch(receivedOrientation){
		case SOUTH:
			return EAST;
		case NORTH:
			return WEST;
		case EAST:
			return NORTH;
		case WEST:
			return SOUTH;
		default:
			return null;
		}
	}
	
	/**
     * Return the result of made a turn right over the received orientation
	 * @param receivedOrientation {@link EOrientation} used to turn right
	 * @return new orientation after turn right
	 */
	public static EOrientation turnRight(EOrientation receivedOrientation){
		switch(receivedOrientation){
		case NORTH:
			return EAST;
		case SOUTH:
			return WEST;
		case EAST:
			return SOUTH;
		case WEST:
			return NORTH;
		default:
			return null;
		}
	}
	
}
