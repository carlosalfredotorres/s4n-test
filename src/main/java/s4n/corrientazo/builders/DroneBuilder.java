package s4n.corrientazo.builders;

import s4n.corrientazo.enums.EOrientation;
import s4n.corrientazo.model.Drone;
import s4n.corrientazo.model.Schedule;

/**
 * Used to build drone objects
 * @author Carlos Torres
 *
 */
public class DroneBuilder {
	
	private int x;
	private int y;
	private EOrientation orientation;
	private int maximumRemote;
	private Schedule schedule;
	private String pathWriteReport;
	
	
	public DroneBuilder setX(int x) {
		this.x = x;
		return this;
	}
	public DroneBuilder setY(int y) {
		this.y = y;
		return this;
	}
	public DroneBuilder setOrientation(EOrientation orientation) {
		this.orientation = orientation;
	    return this;
	}
	public DroneBuilder setMaximumRemote(int maximumRemote) {
		this.maximumRemote = maximumRemote;
        return this;
	}
	public DroneBuilder setSchedule(Schedule schedule) {
		this.schedule = schedule;
		return this;
	}
	public DroneBuilder setPathWriteReport(String pathWriteReport) {
		this.pathWriteReport = pathWriteReport;
		return this;
	}
	public Drone build() {
		return new Drone(x, y, orientation, maximumRemote, schedule, pathWriteReport);
	}

}
