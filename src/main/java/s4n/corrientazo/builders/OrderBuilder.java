package s4n.corrientazo.builders;

import java.util.List;

import s4n.corrientazo.model.Order;

/**
 * Used to build order objects
 * @author Carlos Torres
 *
 */
public class OrderBuilder {
	
	private boolean orderPlaced;
	private String address;
	private List<Character> indications;
	
	public OrderBuilder setOrderPlaced(boolean orderPlaced) {
		this.orderPlaced = orderPlaced;
		return this;
	}
	public OrderBuilder setAddress(String address) {
		this.address = address;
		return this;
	}
	public OrderBuilder setIndications(List<Character> indications) {
		this.indications = indications;
		return this;
	}
	
	
	public Order build() {
		return new Order(orderPlaced, address, indications);
	}

}
