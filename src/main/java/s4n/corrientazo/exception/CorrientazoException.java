package s4n.corrientazo.exception;

/**
 * General custom runtime exception used in all the application
 * @author Carlos Torres
 *
 */
public class CorrientazoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CorrientazoException(String message) {
		super(message);
	}
	
	
}
