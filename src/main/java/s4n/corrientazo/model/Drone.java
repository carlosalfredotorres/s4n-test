package s4n.corrientazo.model;


import s4n.corrientazo.enums.ECustomMessageException;
import s4n.corrientazo.enums.EOrientation;
import s4n.corrientazo.exception.CorrientazoException;
import s4n.util.RestaurantUtils;

/**
 * Used to represent a drone
 * @author Carlos Torres
 *
 */
public class Drone {

	private int x;
	private int y;
	private EOrientation orientation;
	private int maximumRemote;
	private Schedule schedule;
	private String pathWriteReport;
	
	public Drone(int x, int y, EOrientation orientation, int maximumRemote, Schedule schedule,String pathWriteReport) {
		super();
		this.x = x;
		this.y = y;
		this.orientation = orientation;
		this.maximumRemote = maximumRemote;
		this.schedule = schedule;
		this.pathWriteReport = pathWriteReport;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public EOrientation getOrientation() {
		return orientation;
	}
	public void setOrientation(EOrientation orientation) {
		this.orientation = orientation;
	}
	public int getMaximumRemote() {
		return maximumRemote;
	}
	public void setMaximumRemote(int maximumRemote) {
		this.maximumRemote = maximumRemote;
	}
	public Schedule getSchedule() {
		return schedule;
	}
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	public String getPathWriteReport() {
		return pathWriteReport;
	}
	public void setPathWriteReport(String pathWriteReport) {
		this.pathWriteReport = pathWriteReport;
	}
	/**
	 * Sends all the orders schedule for the drone
	 * @return <code>true</code> if all the orders are send, otherwise <code>false</code>
	 */
	public boolean sendOrders() {
		if(schedule == null || schedule.getOrderList().isEmpty()) {
			throw new CorrientazoException(ECustomMessageException.ERROR_NO_SCHEDULE.getMessage());
		}
		
		if(!schedule.isSchedulePlaced()) {
			schedule.getOrderList().stream().forEach( order ->{
				if(!order.isOrderPlaced()) {
					order.getIndications().stream().forEach( (Character move) -> {
						switch (move){
						case 'I':
							setOrientation(EOrientation.turnLeft(getOrientation()));
							break;
						case 'D':
							setOrientation(EOrientation.turnRight(getOrientation()));
							break;
						case 'A':
							goFoward(1);
							break;
						}
						
					});
				}
				order.setAddress(getActualPosition());
				order.setOrderPlaced(true);
			});
		schedule.setSchedulePlaced(true);
		RestaurantUtils.writeReportSchedule(schedule,pathWriteReport);
		return true;
			
		}
		return false;
	}
	
	
	
	/**
	 * Goes foward the drone depending of the number of foward given.
	 * @param foward indicates the number of step to do in the move.
	 */
	public void goFoward(int foward){
		switch (getOrientation()){
			case NORTH:
				y=y+foward;
				break;
			case SOUTH:
				y=y-foward;
				break;
			case EAST:
				x=x-foward;
				break;
			case WEST:
				x=x+foward;
				break;
		}
		if(y>maximumRemote || x>maximumRemote || x<-maximumRemote || y<-maximumRemote){
			throw new CorrientazoException(ECustomMessageException.ERROR_LIMIT_EXCEEDED.getMessage());
		}
	}
	
	/**
	 * Gets the actual position of the drone
	 * @return actual position
	 */
	public String getActualPosition(){
		StringBuilder actualPosition=new StringBuilder();
		actualPosition.append("(").
			append(x).
				append(", ").
					append(y).
						append(") ").
							append(orientation.toString());
		return actualPosition.toString();
	}
	
}
