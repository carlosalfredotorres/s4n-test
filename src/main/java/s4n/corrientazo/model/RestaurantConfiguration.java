package s4n.corrientazo.model;

import java.util.List;

/**
 * Used to represent the configuration for the restaurant
 * 
 * @author Carlos Torres
 *
 */
public class RestaurantConfiguration {

	private int capacitySimultaneousSend;
	private int maximumRemote;
	private int maximumOrderByDrone;
	private List<Schedule> scheduleList;
	
	
	public RestaurantConfiguration(int capacitySimultaneousSend, int maximumRemote, int maximumOrderByDrone,
			List<Schedule> scheduleList) {
		super();
		this.capacitySimultaneousSend = capacitySimultaneousSend;
		this.maximumRemote = maximumRemote;
		this.maximumOrderByDrone = maximumOrderByDrone;
		this.scheduleList = scheduleList;
	}
	public int getCapacitySimultaneousSend() {
		return capacitySimultaneousSend;
	}
	public void setCapacitySimultaneousSend(int capacitySimultaneousSend) {
		this.capacitySimultaneousSend = capacitySimultaneousSend;
	}
	public int getMaximumRemote() {
		return maximumRemote;
	}
	public void setMaximumRemote(int maximumRemote) {
		this.maximumRemote = maximumRemote;
	}
	public int getMaximumOrderByDrone() {
		return maximumOrderByDrone;
	}
	public void setMaximumOrderByDrone(int maximumOrderByDrone) {
		this.maximumOrderByDrone = maximumOrderByDrone;
	}
	public List<Schedule> getScheduleList() {
		return scheduleList;
	}
	public void setScheduleList(List<Schedule> scheduleList) {
		this.scheduleList = scheduleList;
	}

	
	

}
