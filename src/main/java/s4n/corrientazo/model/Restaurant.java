package s4n.corrientazo.model;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import s4n.corrientazo.builders.DroneBuilder;
import s4n.corrientazo.enums.EOrientation;
import s4n.corrientazo.enums.EUrlInputOutput;

import static s4n.corrientazo.enums.ECustomMessageException.*;
import s4n.corrientazo.exception.CorrientazoException;

/**
 * Used to represent the restaurant
 * 
 * @author Carlos Torres
 *
 */
public class Restaurant {

	private RestaurantConfiguration restaurantConfiguration;

	public Restaurant(RestaurantConfiguration restaurantConfiguration) {
		this.restaurantConfiguration = restaurantConfiguration;
	}

	/**
	 * Send all the orders charged in the system.
	 */
	public boolean executeSchedule() {
		
		if(this.restaurantConfiguration==null) {
			throw new CorrientazoException(ERROR_NO_RESTAURANT_CONFIGURATION.getMessage());
		}
		if (this.restaurantConfiguration.getScheduleList() == null || this.restaurantConfiguration.getScheduleList().isEmpty()) {
			throw new CorrientazoException(ERROR_NO_SCHEDULE.getMessage());
		}
		if (!this.restaurantConfiguration.getScheduleList().stream().allMatch(schedule -> schedule.getOrderList().size() <= this.restaurantConfiguration.getMaximumOrderByDrone())) {
			throw new CorrientazoException(
					ORDERS_EXCEEDED.getMessage() + this.restaurantConfiguration.getMaximumOrderByDrone());
		}
        return this.sendSchedule();		
	}

	/**
	 * Configures and executes each schedule 
	 * @return <code>true</code>if all the schedule are sent, otherwise <code>false</code>
	 */
	private boolean sendSchedule() {
		
		ExecutorService executor = Executors.newFixedThreadPool(this.restaurantConfiguration.getCapacitySimultaneousSend());
		
		List<Callable<Boolean>> callableSchedules = this.restaurantConfiguration.getScheduleList().stream()
				.map(this::createScheduleCallable).collect(Collectors.toList());
		
		try {
			List<Future<Boolean>> futureScheduleResults  = executor.invokeAll(callableSchedules);
			
			final boolean isAllSend = futureScheduleResults.stream().allMatch(futureScheduleResult -> {

				try {
					return futureScheduleResult.isDone() && futureScheduleResult.get();
				} catch (InterruptedException | ExecutionException e) {
					throw new CorrientazoException(ERROR_UKNOW.getMessage());
				}
			});
			
			executor.shutdown();
			
			return isAllSend;
		} catch (InterruptedException e) {
			throw new CorrientazoException(ERROR_UKNOW.getMessage());
		}
	}

	/**
	 * Becomes a schedule in a callable object with logic to send the orders.
	 * 
	 * @param schedule {@link Schedule} used to become a callable
	 * @return callable object
	 */
	private Callable<Boolean> createScheduleCallable(Schedule schedule) {
		return () -> {
			Drone drone = new DroneBuilder()
					.setX(0)
					.setY(0)
					.setOrientation(EOrientation.NORTH)
					.setMaximumRemote(this.restaurantConfiguration.getMaximumRemote())
					.setSchedule(schedule)
					.setPathWriteReport(EUrlInputOutput.URL_OUTPUT.getValue())
					.build();
			return drone.sendOrders();
		};
	}
	
}
