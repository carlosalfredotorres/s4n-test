package s4n.corrientazo.model;

import java.util.List;

/**
 * Used to represent the orders in the system.
 * @author Carlos Torres
 *
 */
public class Order {

	private boolean orderPlaced;
	private String address;
	private List<Character> indications;
	
	
	
	public Order(boolean orderPlaced, String address, List<Character> indications) {
		super();
		this.orderPlaced = orderPlaced;
		this.address = address;
		this.indications = indications;
	}

	public Order(){
	}
	

	public boolean isOrderPlaced() {
		return orderPlaced;
	}

	public void setOrderPlaced(boolean orderPlaced) {
		this.orderPlaced = orderPlaced;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Character> getIndications() {
		return indications;
	}

	public void setIndications(List<Character> indications) {
		this.indications = indications;
	}

	
}
