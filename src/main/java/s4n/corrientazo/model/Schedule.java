package s4n.corrientazo.model;

import java.util.List;

/**
 * Represents the schedule by drone
 * @author Carlos Torres
 *
 */
public class Schedule {
	
	private int numberSchedule;
	private boolean schedulePlaced;
	private List<Order> orderList;

	
	public Schedule(List<Order> orderList, int counter) {
		super();
		this.orderList = orderList;
		this.numberSchedule = counter;
	}

	public List<Order> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}

	public boolean isSchedulePlaced() {
		return schedulePlaced;
	}

	public void setSchedulePlaced(boolean schedulePlaced) {
		this.schedulePlaced = schedulePlaced;
	}

	public int getNumberSchedule() {
		return numberSchedule;
	}

	public void setNumberSchedule(int numberSchedule) {
		this.numberSchedule = numberSchedule;
	}
	
	

}
