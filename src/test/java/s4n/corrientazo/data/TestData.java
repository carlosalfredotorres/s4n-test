package s4n.corrientazo.data;

/**
 * Builder class used to create useful data test
 * @author carlos.torres
 *
 */
public class TestData {

	public static final String TEST_DRONE_STRING="(0, 8) dirección Norte";
	public static final String URL_INPUT_TEST = "src\\test\\resources\\in\\";
	public static final int CAPACITY_SIMULTANEOUS_SEND_TEST = 1;
	
	
}
