package s4n.corrientazo.dominio;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static s4n.corrientazo.enums.ERestaurantBasicConfiguration.*;
import static s4n.corrientazo.enums.EUrlInputOutput.*;

import s4n.corrientazo.data.TestData;
import s4n.corrientazo.enums.ECustomMessageException;
import s4n.corrientazo.exception.CorrientazoException;
import s4n.corrientazo.model.Restaurant;
import s4n.corrientazo.model.RestaurantConfiguration;
import s4n.util.RestaurantUtils;

public class RestaurantTest {
 
	
	/**
	 * Tests the execute schedule orders in the restaurant
	 */
	@Test
	public void executeScheduleOrders() {

		RestaurantConfiguration restaurantConfigurationTest = mock(RestaurantConfiguration.class);
		when(restaurantConfigurationTest.getCapacitySimultaneousSend())
				.thenReturn(CAPACITY_SIMULTANEOUS_SEND.getValue());
		when(restaurantConfigurationTest.getMaximumOrderByDrone()).thenReturn(MAXIMUM_ORDER_BY_DRONE.getValue());
		when(restaurantConfigurationTest.getMaximumRemote()).thenReturn(MAXIMUM_REMOTE.getValue());
		when(restaurantConfigurationTest.getScheduleList()).thenReturn(
				RestaurantUtils.getScheduleList(CAPACITY_SIMULTANEOUS_SEND.getValue(), URL_INPUT.getValue()));

		Restaurant restaurantTest = new Restaurant(restaurantConfigurationTest);

		assertTrue(restaurantTest.executeSchedule());

	}

	/**
	 * Tests the execute schedule orders in the restaurant when it has not got configuration
	 */
	@Test
	public void executeScheduleOrdersNoConfiguration() {
		try {

			Restaurant restaurantTest = new Restaurant(null);

			assertTrue(restaurantTest.executeSchedule());

		} catch (CorrientazoException ex) {
			Assert.assertTrue(
					ex.getMessage().equals(ECustomMessageException.ERROR_NO_RESTAURANT_CONFIGURATION.getMessage()));
		}

	}

	/**
	 * Tests the execute schedule orders in the restaurant when it has not got schedules
	 */
	@Test
	public void executeScheduleOrdersNoSchedules() {
		try {

			RestaurantConfiguration restaurantConfigurationTest = mock(RestaurantConfiguration.class);
			when(restaurantConfigurationTest.getCapacitySimultaneousSend())
					.thenReturn(CAPACITY_SIMULTANEOUS_SEND.getValue());
			when(restaurantConfigurationTest.getMaximumOrderByDrone()).thenReturn(MAXIMUM_ORDER_BY_DRONE.getValue());
			when(restaurantConfigurationTest.getMaximumRemote()).thenReturn(MAXIMUM_REMOTE.getValue());
			when(restaurantConfigurationTest.getScheduleList()).thenReturn(null);

			Restaurant restaurantTest = new Restaurant(restaurantConfigurationTest);
			
			assertTrue(restaurantTest.executeSchedule());

		} catch (CorrientazoException ex) {
			Assert.assertTrue(
					ex.getMessage().equals(ECustomMessageException.ERROR_NO_SCHEDULE.getMessage()));
		}

	}
	
	/**
	 * Tests the execute schedule orders in the restaurant when it exceeded the orders by drone
	 */
	@Test
	public void executeScheduleOrdersExceededByDrone() {
		
		RestaurantConfiguration restaurantConfigurationTest = mock(RestaurantConfiguration.class);
		when(restaurantConfigurationTest.getCapacitySimultaneousSend())
				.thenReturn(CAPACITY_SIMULTANEOUS_SEND.getValue());
		when(restaurantConfigurationTest.getMaximumOrderByDrone()).thenReturn(MAXIMUM_ORDER_BY_DRONE.getValue());
		when(restaurantConfigurationTest.getMaximumRemote()).thenReturn(MAXIMUM_REMOTE.getValue());
		when(restaurantConfigurationTest.getScheduleList()).thenReturn(
				RestaurantUtils.getScheduleList(TestData.CAPACITY_SIMULTANEOUS_SEND_TEST, TestData.URL_INPUT_TEST));
		
		try {
			Restaurant restaurantTest = new Restaurant(restaurantConfigurationTest);
			
			assertTrue(restaurantTest.executeSchedule());

		} catch (CorrientazoException ex) {
			Assert.assertTrue(
					ex.getMessage().equals(ECustomMessageException.ORDERS_EXCEEDED.getMessage()+ restaurantConfigurationTest.getMaximumOrderByDrone()));
		}

	}

}
