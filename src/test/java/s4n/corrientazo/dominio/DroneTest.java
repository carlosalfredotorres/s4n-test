package s4n.corrientazo.dominio;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import s4n.corrientazo.builders.DroneBuilder;
import s4n.corrientazo.data.TestData;
import s4n.corrientazo.enums.ECustomMessageException;
import s4n.corrientazo.enums.EOrientation;
import s4n.corrientazo.enums.EUrlInputOutput;
import s4n.corrientazo.exception.CorrientazoException;
import s4n.corrientazo.model.Drone;
import s4n.corrientazo.model.Schedule;
import s4n.util.RestaurantUtils;

/**
 * Test class used to test the drone features
 * @author carlos.torres
 *
 */
public class DroneTest {

	    /**
	     * Tests the capacity to move of a drone
	     */
	    @Test
		public void goFowardDroneTest(){
			Drone droneTest = new DroneBuilder()
					.setMaximumRemote(10)
					.setOrientation(EOrientation.NORTH)
					.setX(0)
					.setY(0)
					.build();
			
			droneTest.goFoward(8);
			Assert.assertTrue(droneTest.getActualPosition().equals(TestData.TEST_DRONE_STRING));
		}
		
	    /**
	     * Tests the maximum remote that drone can get
	     */
		 @Test
		public void goFowardDroneExceededMaximumRemoteTest(){
			 try{
				 Drone droneTest = new DroneBuilder()
							.setMaximumRemote(10)
							.setOrientation(EOrientation.NORTH)
							.setX(0)
							.setY(0)
							.build();
				
				droneTest.goFoward(15);
				fail();
			 }catch(CorrientazoException ex){
				 Assert.assertTrue(ex.getMessage().equals(ECustomMessageException.ERROR_LIMIT_EXCEEDED.getMessage()));
			 }
		}
		
		 /**
		 * Tests the capacity of a drone to send orders
		 */
		 @Test
		 public void sendOrdersTest() {
			 
			 Schedule scheduleTest = RestaurantUtils.getScheduleList(1, EUrlInputOutput.URL_INPUT.getValue()).get(0);
			 
			 Drone droneTest = new DroneBuilder()
						.setMaximumRemote(10)
						.setOrientation(EOrientation.NORTH)
						.setX(0)
						.setY(0)
						.setPathWriteReport(EUrlInputOutput.URL_OUTPUT.getValue())
						.setSchedule(scheduleTest)
						.build();
			 
			 boolean isOrderSent = droneTest.sendOrders();
			 Assert.assertTrue(isOrderSent);
			 
		 }
		 
		 /**
		  * Tests the capacity or reaction that a drone has when it has not got a schedule
		  */
		 @Test
		 public void sendOrdersNoScheduleTest() {
			 
			 try {
			 Schedule scheduleTest = mock(Schedule.class);
			 when(scheduleTest.getOrderList()).thenReturn(new ArrayList<>());
			 when(scheduleTest.getNumberSchedule()).thenReturn(1);
			 
			 Drone droneTest = new DroneBuilder()
						.setMaximumRemote(10)
						.setOrientation(EOrientation.NORTH)
						.setX(0)
						.setY(0)
						.setPathWriteReport(EUrlInputOutput.URL_OUTPUT.getValue())
						.setSchedule(scheduleTest)
						.build();
			 
			  droneTest.sendOrders();
			  fail();
			 }catch(CorrientazoException ex){
				 Assert.assertTrue(ex.getMessage().equals(ECustomMessageException.ERROR_NO_SCHEDULE.getMessage()));
			 }
		 }
	
}
